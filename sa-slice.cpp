#include <iostream>
#include <string>
#include <list>
#include <vector>
 
template <typename T>
struct array_wrapper
{
    private:
        T* data_;
        size_t size_;
    public:
        typedef T* iterator;
        typedef const T* const_iterator;
        array_wrapper(T* begin, T* end) : data_(begin), size_(end-begin) {}
        iterator begin() { return data_; }
        iterator end() { return data_ + size_; }
        const_iterator begin() const { return data_; }
        const_iterator end() const { return data_ + size_; }
        size_t size() const { return size_; }
        T& operator[](size_t i) { return data_[i]; }
        const T& operator[](size_t i) const { return data_[i]; }
        void swap(array_wrapper& other)
        {
            std::swap(size_, other.size_);
            std::swap(data_, other.data_);
        }
};
 
template <typename T>
class array_slicer
{
    public:
        static void slice(std::vector<T>* v, unsigned int n, std::list<array_wrapper<T>*>* l)
        {
            auto _size = v->size() / n;
            auto _last = v->size() % n;
            auto _ptr = &v->front();
            for (int i = 0; i < n; i++) {
                array_wrapper<T>* s = new array_wrapper<T>(_ptr, _ptr + _size);
                l->push_back(s);
                _ptr += _size;
            }
            if (_last > 0) {
                array_wrapper<T>* slice = new array_wrapper<T>(_ptr, _ptr + _last);
                l->push_back(slice);
            }
        }
};
 
int main()
{
    //get vector to slice
    int s = {0};
    std::cout << "Size:";
    std::cin >> s;
    auto _vector = new std::vector<int>(s);
    for (int i = 0; i < _vector->size(); i++)
    {
        std::vector<int> &v = *_vector;
        v[i] = i;
    }
    //get slices parameters
    int n = {0};
    std::cout << "Slices:";
    std::cin >> n;
    //create list of slices
    auto _list = new std::list<array_wrapper<int>*>();
    array_slicer<int>::slice(_vector, n, _list);
    //output result
    for (auto iter = _list->begin(); iter != _list->end(); iter++)
    {
        for (auto iterv = (*iter)->begin(); iterv != (*iter)->end(); iterv++) {
            std::cout << (*iterv) << ", ";
        }
        std::cout << "size: " << (*iter)->size() << "\n";
    }
}
 
// Size:10                                                                                                                                                      
// Slices:3                                                                                                                                                      
// 0, 1, 2, size: 3                                                                                                                                              
// 3, 4, 5, size: 3                                                                                                                                              
// 6, 7, 8, size: 3                                                                                                                                              
// 9, size: 1
