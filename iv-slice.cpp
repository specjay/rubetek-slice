#include <iostream>
#include <vector>

int main()
{
	std::vector<int> v = { 53245,6236,342,2345,346,34,2437,327,4327,2346 };
	int n = 3;
	int size = ((v.size() - 1) / n) + 1;
	std::vector<int> chunks[size];
	
	for (int j = 0; j < size; ++j)
	{
		auto start = std::next(v.cbegin(), j*n);
		auto end = std::next(v.cbegin(), j*n + n);
		
		chunks[j].resize(n);
		
		if (((j*n) + n) > v.size()) 
		{
			end = v.cend();
			chunks[j].resize(v.size() - j*n);
		}

		std::copy(start, end, chunks[j].begin());
	}
	
	for (auto &chunk : chunks)
	{
	    for(auto &chunkelem : chunk)
	    {
	        std::cout << chunkelem << " ";
	    }
	    std::cout << std::endl;
	}
	
	return 0;
}

