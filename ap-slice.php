<html>
  <head>
    <title>My each slice</title>
    <style>
      body {
        font-family: monospace;
        font-size: 16px;
        background: #111;
        color: #eee;
      }
    </style>
  </head>
  <body>
    <?php 
      function my_slice($slice, $array) {
        if($slice <= 0){
          echo 'Slice should be >0';
        } else if (is_array($array) == 0){ 
          echo 'Invalid array';
        } else { 
          $array_size = count($array);
          echo '[';

          for ($i = 1; $i <= $array_size; $i++) {
            echo $array[$i - 1];
       
            if (($i % $slice == 0) && ($i != $array_size)) {
              echo ']<br>[';
            } else if ($i != $array_size){
              echo ', ';
            }
          }
          
          echo ']';
        }
      }
      
      $my_array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
      my_slice(3, $my_array);
    ?> 
  </body>
</html>
